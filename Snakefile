import pandas as pd

WORKING_DIR = "/fast/users/helmsauk_c/work/nb-cl-pe-chipseq/"
FASTQ_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-pe-chipseq/fastq/"
BAM_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-pe-chipseq/bam/"
MACS2_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-pe-chipseq/MACS2/"
MACS2_BROAD_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-pe-chipseq/MACS2_broad/"

REFERENCE_FASTA = "/fast/users/helmsauk_c/scratch/hg19_bwa/hg19.fa"
BBMAP_DIR = "/fast/users/helmsauk_c/work/miniconda/envs/qc/opt/bbmap-38.58-0/"
ADAPTER_SEQUENCES = BBMAP_DIR + "resources/adapters.fa"
PICARD_JAR = '/fast/users/helmsauk_c/work/Applications/picard-2.20.4/picard.jar'
TMP_DIR = "/fast/users/helmsauk_c/scratch/tmp"
SAMTOOLS_PATH = "/fast/users/helmsauk_c/work/miniconda/bin/samtools"
# mkdir /fast/users/helmsauk_c/work/resources/blacklists/ && cd /fast/users/helmsauk_c/work/resources/blacklists/
# wget http://hgdownload.cse.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeMapability/wgEncodeDacMapabilityConsensusExcludable.bed.gz
# gunzip wgEncodeDacMapabilityConsensusExcludable.bed.gz
# bedtools merge -i wgEncodeDacMapabilityConsensusExcludable.bed > wgEncodeDacMapabilityConsensusExcludable.mergeBed.bed
MAPPABILITY_BLACKLIST = "/fast/users/helmsauk_c/work/resources/blacklists/wgEncodeDacMapabilityConsensusExcludable.mergeBed.bed"

metadata = pd.read_csv(WORKING_DIR + "metadata.csv", sep=";")
metadata_withcontrol = metadata[metadata.CONTROL.notnull()]
EXP = metadata.SAMPLE.tolist()
TREATMENT_TO_CONTROL = pd.Series(metadata_withcontrol.CONTROL.values,index=metadata_withcontrol.SAMPLE).to_dict()
TREATMENTS = list(TREATMENT_TO_CONTROL.keys())

rule all:
    input:
        expand(FASTQ_DIR + "{exp}.{r}_fastqc.html", exp=EXP, r = ["R1", "R2"]),
        #expand(FASTQ_DIR + "{exp}.{r}.trimmed_fastqc.html", exp=EXP, r = ["R1", "R2"]),
        expand(BAM_DIR + "{exp}.trimmed.hg19.rmdup.bam", exp=EXP),
        expand(BAM_DIR + "{exp}.trimmed.hg19.rmdup.bw", exp=EXP),
        expand(BAM_DIR + "{exp}.trimmed.hg19.rmdup.filterednormed.bw", exp=EXP),
        expand(BAM_DIR + "{exp}.trimmed.hg19.rmdup.bam.{r}.qc.txt", exp=EXP, r = ["R1", "R2"]),
        expand(MACS2_DIR + "{treatment}/{treatment}_MACS2_peaks.narrowPeak", treatment=TREATMENTS),
        expand(MACS2_BROAD_DIR + "{treatment}/{treatment}_MACS2_broad_peaks.broadPeak", treatment=TREATMENTS),
        expand(MACS2_DIR + "{exp}/{exp}_MACS2_nocontrol_peaks.narrowPeak", exp=EXP),
        expand(MACS2_BROAD_DIR + "{exp}/{exp}_MACS2_nocontrol_broad_peaks.broadPeak", exp=EXP),
        WORKING_DIR + "multiqc_report.html"

rule fastqc:
    input:
        "{exp}.fq.gz"
    output:
        "{exp}_fastqc.html"
    conda:
        "envs/qc.yaml"
    shell:
        "fastqc {input}"

rule adapter_trimming:
    input:
        r1 = FASTQ_DIR + "{exp}.R1.fq.gz",
        r2 = FASTQ_DIR + "{exp}.R2.fq.gz"
    output:
        r1_trimmed = FASTQ_DIR + "{exp}.R1.trimmed.fq.gz",
        r2_trimmed = FASTQ_DIR + "{exp}.R2.trimmed.fq.gz"
    conda:
        "envs/qc.yaml"
    shell:
        BBMAP_DIR + "bbduk.sh in1={input.r1} in2={input.r2} out1={output.r1_trimmed} out2={output.r2_trimmed} ktrim=r k=23 mink=11 hdist=1 ref=" + ADAPTER_SEQUENCES + " tbo qtrim=r trimq=15 maq=20"

rule bwa_index:
    input:
        REFERENCE_FASTA
    output:
        REFERENCE_FASTA + ".bwt"
    params:
        samtools_bin = SAMTOOLS_PATH
    conda:
        "envs/bwa.yaml"
    shell:
        "{params.samtools_bin} faidx {input} && bwa index {input}"

rule alignment:
    input:
        ref = REFERENCE_FASTA,
        ref_bwa_index = REFERENCE_FASTA + ".bwt",
        r1 = FASTQ_DIR +  "{exp}.R1.trimmed.fq.gz",
        r2 = FASTQ_DIR +  "{exp}.R2.trimmed.fq.gz"
    output:
        bam = BAM_DIR + "{exp}.trimmed.hg19.bam"
    conda:
        "envs/bwa.yaml"
    params:
        threads = "10",
        samtools_bin = SAMTOOLS_PATH
    shell:
        "bwa mem {input.ref} {input.r1} {input.r2} -t {params.threads} | {params.samtools_bin} sort -@{params.threads} -o {output.bam} - && {params.samtools_bin} index {output.bam} && {params.samtools_bin} stats {output.bam} > {output.bam}.stats.txt"

# Thought about including a | samtools view -h -f 3 -F 4 -F 8 -F 256 -F 2048 in the pipeline above. Should I do that?
# -f 3: only include alignments marked with the SAM flag 3, which means "properly paired and mapped"
# -F 4: exclude aligned reads with flag 4: the read itself did not map
# -F 8: exclude aligned reads with flag 8: their mates did not map
# -F 256: exclude alignments with flag 256, which means that bwa mapped the read to multiple places in the reference genome, and this alignment is not the best
# -F 1024: exclude alignments marked with SAM flag 1024, which indicates that the read is an optical or PCR duplicate (this flag would be set by Picard)
# -F 2048: filter out chimeric reads
# -q 30: minimum mapping quality 30

rule remove_duplicates:
    input:
        bam = BAM_DIR + "{exp}.trimmed.hg19.bam"
    output:
        bam_markdup = BAM_DIR + "{exp}.trimmed.hg19.rmdup.bam",
        bam_markdup_metrics = BAM_DIR + "{exp}.trimmed.hg19.rmdup-metrics.txt"
    params:
        tmp_dir = TMP_DIR,
        picard_jar = PICARD_JAR
    conda:
        "envs/arima-mapping.yaml"
    shell:
        "java -Xmx30G -XX:-UseGCOverheadLimit -Djava.io.tmpdir={params.tmp_dir} -jar {params.picard_jar} MarkDuplicates INPUT={input.bam} OUTPUT={output.bam_markdup} METRICS_FILE={output.bam_markdup_metrics} TMP_DIR={params.tmp_dir} REMOVE_DUPLICATES=TRUE MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 CREATE_INDEX=TRUE"

rule phantompeakqualtools_r1:
    input:
        BAM_DIR + "{exp}.trimmed.hg19.rmdup.bam"
    output:
        BAM_DIR + "{exp}.trimmed.hg19.rmdup.bam.R1.qc.txt"
    conda:
        "envs/phantompeakqualtools.yaml"
    shell:
        "run_spp.R -rf -c=(<samtools view -b -f 0x1 -f 0x40 {input}) -savp -out={input}.R1.qc.txt"

rule phantompeakqualtools_r2:
    input:
        BAM_DIR + "{exp}.trimmed.hg19.rmdup.bam"
    output:
        BAM_DIR + "{exp}.trimmed.hg19.rmdup.bam.R2.qc.txt"
    conda:
        "envs/phantompeakqualtools.yaml"
    shell:
        "run_spp.R -rf -c=(<samtools view -b -f 0x1 -f 0x80 {input}) -savp -out={input}.R2.qc.txt"

rule raw_bigWig:
    input:
        BAM_DIR + "{exp}.trimmed.hg19.rmdup.bam"
    output:
        both_strands = BAM_DIR + "{exp}.trimmed.hg19.rmdup.bw",
        fwd_strand = BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.fwd_strand.bw",
        rev_strand = BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.rev_strand.bw"
    conda:
        "envs/deeptools.yaml"
    shell:
        "bamCoverage --bam {input} -o {output.both_strands} && bamCoverage -b {input} -o {output.fwd_strand} --samFlagExclude 16 && bamCoverage -b {input} -o {output.rev_strand} --samFlagInclude 16"

rule filtered_bigWig:
    input:
        BAM_DIR + "{exp}.trimmed.hg19.rmdup.bam"
    output:
        BAM_DIR + "{exp}.trimmed.hg19.rmdup.filterednormed.bw"
    params:
        blacklist = MAPPABILITY_BLACKLIST
    conda:
        "envs/deeptools.yaml"
    shell:
        "bamCoverage --bam {input} -o {output} --blackListFileName {params.blacklist} --extendReads --normalizeUsing CPM -ignore chrM --binSize 10"

rule macs2_narrowPeak_controlfree:
    input:
        BAM_DIR + "{sample}.trimmed.hg19.rmdup.bam"
    output:
        MACS2_DIR + "{sample}/{sample}_MACS2_nocontrol_peaks.narrowPeak"
    params:
        outdir = MACS2_DIR + "{sample}/",
        sample = "{sample}_MACS2_nocontrol"
    conda:
        "envs/macs2.yaml"
    shell:
        "macs2 callpeak --treatment {input} --name {params.sample} --outdir {params.outdir} --format BAMPE"

rule macs2_broadPeak_controlfree:
    input:
        BAM_DIR + "{sample}.trimmed.hg19.rmdup.bam"
    output:
        MACS2_BROAD_DIR + "{sample}/{sample}_MACS2_nocontrol_broad_peaks.broadPeak"
    params:
        outdir = MACS2_BROAD_DIR + "{sample}/",
        sample = "{sample}_MACS2_nocontrol_broad"
    conda:
        "envs/macs2.yaml"
    shell:
        "macs2 callpeak --treatment {input} --name {params.sample} --outdir {params.outdir} --broad --format BAMPE"

rule macs2_narrowPeak:
    input:
        treatment = BAM_DIR + "{sample}.trimmed.hg19.rmdup.bam",
        control = lambda wildcards: BAM_DIR + TREATMENT_TO_CONTROL[wildcards.sample] + ".trimmed.hg19.rmdup.bam"
    output:
        MACS2_DIR + "{sample}/{sample}_MACS2_peaks.narrowPeak"
    params:
        outdir = MACS2_DIR + "{sample}/",
        sample = "{sample}",
    conda:
        "envs/macs2.yaml"
    shell:
        "macs2 callpeak --treatment {input.treatment} --control {input.control} --name {params.sample}_MACS2 --outdir {params.outdir} --format BAMPE"

rule macs2_broadPeak:
    input:
        treatment = BAM_DIR + "{sample}.trimmed.hg19.rmdup.bam",
        control = lambda wildcards: BAM_DIR + TREATMENT_TO_CONTROL[wildcards.sample] + ".trimmed.hg19.rmdup.bam"
    output:
        MACS2_BROAD_DIR + "{sample}/{sample}_MACS2_broad_peaks.broadPeak"
    params:
        outdir = MACS2_BROAD_DIR + "{sample}/",
        sample = "{sample}_MACS2_broad"
    conda:
        "envs/macs2.yaml"
    shell:
        "macs2 callpeak --treatment {input.treatment} --control {input.control} --name {params.sample} --outdir {params.outdir} --broad --format BAMPE"

rule peak_exclude_blacklist:
    input:
        "{peakfile}Peak"
    output:
        "{peakfile}Peak.filtered.bed"
    params:
        blacklist = MAPPABILITY_BLACKLIST
    conda:
        "envs/bedtools.yaml"
    shell:
        "bedtools intersect -a {input} -b {params.blacklist} -v > {output}"

rule multiqc:
    input:
        expand(FASTQ_DIR + "{exp}.{r}_fastqc.html", exp=EXP, r=["R1", "R2"]),
        #expand(FASTQ_DIR + "{exp}.{r}.trimmed_fastqc.html", exp=EXP, r=["R1", "R2"]),
        expand(BAM_DIR + "{exp}.trimmed.hg19.rmdup.bw", exp=EXP),
        expand(MACS2_DIR + "{treatment}/{treatment}_MACS2_peaks.narrowPeak", treatment=TREATMENTS),
        expand(MACS2_BROAD_DIR + "{treatment}/{treatment}_MACS2_broad_peaks.broadPeak", treatment=TREATMENTS),
        expand(MACS2_DIR + "{exp}/{exp}_MACS2_nocontrol_peaks.narrowPeak", exp=EXP),
        expand(MACS2_BROAD_DIR + "{exp}/{exp}_MACS2_nocontrol_broad_peaks.broadPeak", exp=EXP)
    output:
        WORKING_DIR + "multiqc_report.html"
    conda:
        "envs/qc.yaml"
    shell:
        "multiqc -f " + FASTQ_DIR + " " + BAM_DIR + " " + MACS2_DIR
